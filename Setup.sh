#!/bin/bash

# Inspired  by HyprV3 By SolDoesTech - https://www.youtube.com/@SolDoesTech, https://github.com/JoshM-Yoru/dotfiles and https://github.com/linuxmobile/hyprland-dots/tree/Sakura
# License..? - You may copy, edit and distribute this script any way you like, enjoy! :)

# set some colors
CNT="[\e[1;36mNOTE\e[0m]"
COK="[\e[1;32mOK\e[0m]"
CER="[\e[1;31mERROR\e[0m]"
CAT="[\e[1;37mATTENTION\e[0m]"
CWR="[\e[1;35mWARNING\e[0m]"
CAC="[\e[1;33mACTION\e[0m]"
INSTLOG="install.log"

# clear the screen
clear

# set some expectations for the user
echo -e "$CNT - You are about to execute a script that would attempt to setup Hyprland.
Please note that Hyprland is still in Beta."
sleep 1

# attempt to discover if this is a VM or not
echo -e "$CNT - Checking for Physical or VM..."
ISVM=$(hostnamectl | grep Chassis)
echo -e "Using $ISVM"
if [[ $ISVM == *"vm"* ]]; then
	echo -e "$CWR - Please note that VMs are not fully supported and if you try to run this on
    a Virtual Machine there is a high chance this will fail."
	sleep 1
fi

# let the user know that we will use sudo
echo -e "$CNT - This script will run some commands that require sudo. You will be prompted to enter your password.
If you are worried about entering your password then you may want to review the content of the script."
sleep 1

# give the user an option to exit out
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to continue with the install (Y,n) ' CONTINST
if [[ $CONTINST == "n" || $CONTINST == "N" ]]; then
	echo -e "$CNT - This script would now exit, no changes were made to your system."
	exit
else
	echo -e "$CNT - Setup starting..."
fi

# Ask if the use has an NVIDIA GPU
read -rep $'[\e[1;33mACTION\e[0m] - Do you have an Nvidia GPU? (Y,n) ' ISNVIDIA
if [[ $ISNVIDIA == "N" || $ISNVIDIA == "n" ]]; then
	ISNVIDIA=false
else
	echo -e "$CWR - Please note that support for Nvidia GPUs is limited.
    This script would attempt to set things up the best way it can.
    If you do end up with a black screen after trying to login then the GPU is likely the issue."

	ISNVIDIA=true
fi

### Disable wifi powersave mode ###
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to disable WiFi powersave? (Y,n) ' WIFI
if [[ $WIFI == "N" || $WIFI == "n" ]]; then
	echo -e "$COK - WiFi powersave won't be disabled, moving on."
else
	LOC="/etc/NetworkManager/conf.d/wifi-powersave.conf"
	echo -e "$CNT - The following file has been created $LOC."
	echo -e "[connection]\nwifi.powersave = 2" | sudo tee -a $LOC &>>$INSTLOG
	echo -e "\n"
	echo -e "$CNT - Restarting NetworkManager service..."
	sleep 1
	sudo systemctl restart NetworkManager &>>$INSTLOG
	sleep 3
fi

#### Check for package manager ####
ISPARU=/sbin/paru
if [ -f "$ISPARU" ]; then
	echo -e "$COK - paru was located, moving on."
else
	echo -e "$CWR - Paru was NOT located.. paru is (still) required"
	read -rep $'[\e[1;33mACTION\e[0m] - Would you like to install paru (Y,n) ' INSTPARU
	if [[ $INSTPARU == "n" || $INSTPARU == "N" ]]; then # no package found so installing
		echo -e "$CER - Paru is (still) required for this script, now exiting"
		exit
	else
		echo -e "$CNT - Installing base-devel ..."
		pacman -S --noconfirm base-devel &>>$INSTLOG
		echo -e "$CNT - Installing Paru ..."
		git clone https://aur.archlinux.org/paru.git &>>$INSTLOG
		cd paru
		makepkg -si --noconfirm &>>../$INSTLOG
		cd ..
	fi
	# update the paru database
	echo -e "$CNT - Updating the paru database..."
	paru -Suy --noconfirm &>>$INSTLOG
fi

# function that will test for a package and if not found it will attempt to install it
install_software() {
	# First lets see if the package is there
	if paru -Q $1 &>>/dev/null; then
		echo -e "$COK - $1 is already installed."
	else
		# no package found so installing
		echo -e "$CNT - Now installing $1 ..."
		paru -S --noconfirm $1 &>>$INSTLOG
		# test to make sure package installed
		if paru -Q $1 &>>/dev/null; then
			echo -e "\e[1A\e[K$COK - $1 was installed."
		else
			# if this is hit then a package is missing, exit to review log
			echo -e "\e[1A\e[K$CER - $1 install had failed, please check the install.log"
			exit
		fi
	fi
}

### Install all of the above pacakges ####
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to install the packages? (Y,n) ' INST
if [[ $INST == "N" || $INST == "n" ]]; then
	echo -e "$COK - Packages won't be installed, moving on."
else
	read -rep $'[\e[1;33mACTION\e[0m] - What is your kernel? (1. Linux (default), 2. Linux-LTS) ' LIN
	if [[ "$LIN" == 2 ]]; then
		# Setup Nvidia if it was selected
		if [[ "$ISNVIDIA" == true ]]; then
			echo -e "$CNT - Nvidia setup stage, this may take a while..."
			for SOFTWR in linux-lts-headers nvidia-dkms qt5-wayland qt5ct libva libva-nvidia-driver-git; do
				install_software $SOFTWR
			done

			# update config
			sudo sed -i 's/MODULES=()/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)/' /etc/mkinitcpio.conf
			sudo mkinitcpio --config /etc/mkinitcpio.conf --generate /boot/initramfs-custom.img
			echo -e "options nvidia-drm modeset=1" | sudo tee -a /etc/modprobe.d/nvidia.conf &>>$INSTLOG
		fi
	else
		# Setup Nvidia if it was selected
		if [[ "$ISNVIDIA" == true ]]; then
			echo -e "$CNT - Nvidia setup stage, this may take a while..."
			for SOFTWR in linux-headers nvidia-dkms qt5-wayland qt5ct libva libva-nvidia-driver-git; do
				install_software $SOFTWR
			done

			# update config
			sudo sed -i 's/MODULES=()/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)/' /etc/mkinitcpio.conf
			sudo mkinitcpio --config /etc/mkinitcpio.conf --generate /boot/initramfs-custom.img
			echo -e "options nvidia-drm modeset=1" | sudo tee -a /etc/modprobe.d/nvidia.conf &>>$INSTLOG
		fi
	fi

		# Stage 1 - main components
		echo -e "$CNT - Stage 1 - Installing main components, this may take a while..."
			for SOFTWR in hyprland polkit-gnome kitty xdg-desktop-portal-hyprland hyprpicker nwg-look wlogout wlsunset swaylock-effects swayidle pamixer pavucontrol brightnessctl bluez bluez-utils blueman network-manager-applet btop cliphist udiskie aylurs-gtk-shell swaybg pipewire pipewire-alsa pipewire-audio pipewire-pulse pipewire-jack wireplumber gst-plugin-pipewire ranger; do
				install_software $SOFTWR
			done
		# Stage 2 - more tools
		echo -e "$CNT - Stage 2 - Installing additional tools and utilities, this may take a while..."
		for SOFTWR in python-requests gvfs thunar-archive-plugin file-roller neofetch neovim tumbler ripgrep jq colord ffmpegthumbnailer gnome-keyring gtk-engine-murrine imagemagick kvantum playerctl qt5-quickcontrols qt5-quickcontrols2 qt5-wayland qt6-wayland xwaylandvideobridge-git qt5-imageformats qt5ct noise-suppression-for-voice lazygit acpi downgrade; do
			install_software $SOFTWR
		done

		# Stage 3 - some visual tools
		echo -e "$CNT - Stage 3 - Installing theme and visual related tools and utilities, this may take a while..."
		for SOFTWR in starship ttf-jetbrains-mono-nerd noto-fonts-emoji lxappearance xfce4-settings sddm-git sddm-sugar-candy-git otf-font-awesome ttf-font-awesome ttf-jetbrains-mono ttf-icomoon-feather cava catppuccin-gtk-theme-macchiato papirus-icon-theme kvantum kvantum-theme-catppuccin-git; do
			install_software $SOFTWR
		done

		# Start the bluetooth service
		echo -e "$CNT - Starting the Bluetooth Service..."
		sudo systemctl enable --now bluetooth.service &>>$INSTLOG
		sleep 2

		# Enable the sddm login manager service
		echo -e "$CNT - Enabling the SDDM Service..."
		sudo systemctl enable sddm &>>$INSTLOG
		sleep 2

		# Clean out other portals
		echo -e "$CNT - Cleaning out conflicting xdg portals..."
		paru -R --noconfirm xdg-desktop-portal-gnome xdg-desktop-portal-gtk &>>$INSTLOG

		### Copy Config Files ###
		read -rep $'[\e[1;33mACTION\e[0m] - Would you like to copy config files? (Y,n) ' CFG
		if [[ $CFG == "N" || $CFG == "n" ]]; then
			echo -e "$CNT - Files won't be copied, moving on."
		else
			echo -e "$CNT - Copying config files..."

			# copy the Hypr directory
			mkdir -p ~/.config
			mkdir -p ~/Pictures
			mkdir -p ~/Downloads
			mkdir -p ~/Documents
			mkdir -p ~/.themes
			mkdir -p ~/.icons
			cp -R Extras/icons/* ~/.icons
			cp -R Extras/themes/* ~/.themes
			cp -R Extras/Wallpapers ~/Pictures/
			cp -R Extras/kitty ~/.config/
			cp -R Extras/nvim ~/.config/
			cp -R Extras/neofetch ~/.config/
			cp -R Hypr/gtklock/ ~/.config/
			cp -R Hypr/hypr/ ~/.config/
			cp -R Hypr/ags/ ~/.config/
			cp -R Hypr/swayidle/ ~/.config/
			cp -R Hypr/swaylock/ ~/.config/
			cp -R Hypr/wlogout/ ~/.config/

			# Copy the SDDM theme
			echo -e "$CNT - Setting up the login screen."
			sudo mkdir -p /etc/sddm.conf.d
			echo -e "[Theme]\nCurrent=sugar-candy" | sudo tee -a /etc/sddm.conf.d/10-theme.conf &>>$INSTLOG
			WLDIR=/usr/share/wayland-sessions
			if [ -d "$WLDIR" ]; then
				echo -e "$COK - $WLDIR found"
			else
				echo -e "$CWR - $WLDIR NOT found, creating..."
				sudo mkdir $WLDIR
			fi

		### Install the starship shell ###
		read -rep $'[\e[1;33mACTION\e[0m] - Would you like to activate the starship shell? (Y,n) ' STAR
		if [[ $STAR == "N" || $STAR == "n" ]]; then
			echo -e "$COK - Starship won't be activated, moving on."
		else
			# install the starship shell
			echo -e "$CNT - Hansen Crusher, Engage!"
			echo -e "$CNT - Updating .bashrc..."
			echo -e '\neval "$(starship init bash)"' >>~/.bashrc
			echo -e "$CNT - copying starship config file to ~/.confg ..."
			cp -rd Extras/starship/ ~/.config/
		fi
		### Install ZSH ###
		read -rep $'[\e[1;33mACTION\e[0m] - Would you like to activate ZSH? (Y,n) ' ZSH
		if [[ $ZSH == "N" || $ZSH == "n" ]]; then
			echo -e "$COK - ZSH won't be activated, installing bash completion."
			install_software bash-completion
		else
			# install ZSH
			for SOFTWR in zsh zsh-autocomplete-git zsh-autopair-git zsh-autosuggestions zsh-fzf-plugin-git zsh-syntax-highlighting zsh-you-should-use fzf-tab-git git-completion; do
				install_software $SOFTWR
			done
			chsh -s /usr/bin/zsh
			cp -a Extras/zsh/. ~/
		fi
	fi
fi

### Install security softwares ###
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to install your security softwares? (Y,n) ' SEC
if [[ $SEC == "N" || $SEC == "n" ]]; then
	echo -e "$COK - Packages won't be installed, moving on."
else
	echo -e "$CNT - Update the system..."
	sudo pacman -Suy --noconfirm &>>$INSTLOG

	echo -e "$CNT - Installing Security pacakges..."
	for SOFTWR in ufw gufw timeshift timeshift-autosnap gparted; do
		install_software $SOFTWR
	done
	sudo systemctl enable ufw
fi

### Install dev softwares ###
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to install your dev softwares? (Y,n) ' DEV
if [[ $DEV == "N" || $DEV == "n" ]]; then
	echo -e "$COK - Packages won't be installed, moving on."
else
	echo -e "$CNT - Update the system..."
	sudo pacman -Suy --noconfirm &>>$INSTLOG

	echo -e "$CNT - Installing Dev pacakges..."
	for SOFTWR in nodejs yarn libreoffice-fresh obsidian postman-bin; do
		install_software $SOFTWR
	done
	mkdir -p ~/Documents/Dev
fi

### Install Virtualization softwares ###
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to install your virtualization softwares? (Y,n) ' VIRT
if [[ $VIRT == "N" || $VIRT == "n" ]]; then
	echo -e "$COK - Packages won't be installed, moving on."
else
	echo -e "$CNT - Update the system..."
	sudo pacman -Suy --noconfirm &>>$INSTLOG

	echo -e "$CNT - Installing Virtualization pacakges..."
	for SOFTWR in docker docker-compose helm kubectl; do
		install_software $SOFTWR
	done

	echo -e "$CNT - Configuring Docker..."
	# sudo groupadd docker
	sudo usermod -aG docker $USER
	newgrp docker
fi

### Install Design softwares ###
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to install your design softwares? (Y,n) ' DESIGN
if [[ $DESIGN == "N" || $DESIGN == "n" ]]; then
	echo -e "$COK - Packages won't be installed, moving on."
else
	echo -e "$CNT - Update the system..."
	sudo pacman -Suy --noconfirm &>>$INSTLOG

	echo -e "$CNT - Installing Dev pacakges..."
	for SOFTWR in gimp inkscape figma-linux-bin; do
		install_software $SOFTWR
	done
fi

### Install Internet softwares ###
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to install your internet softwares? (Y,n) ' INTERNET
if [[ $INTERNET == "N" || $INTERNET == "n" ]]; then
	echo -e "$COK - Packages won't be installed, moving on."
else
	echo -e "$CNT - Update the system..."
	sudo pacman -Suy --noconfirm &>>$INSTLOG

	echo -e "$CNT - Installing Internet pacakges..."
	for SOFTWR in brave-bin webcord firefox thunderbird google-chrome qbittorrent whatsapp-nativefier slack-desktop; do
		install_software $SOFTWR
	done
fi

### Install Media softwares ###
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to install your media softwares? (Y,n) ' MEDIA
if [[ $MEDIA == "N" || $MEDIA == "n" ]]; then
	echo -e "$COK - Packages won't be installed, moving on."
else
	echo -e "$CNT - Update the system..."
	sudo pacman -Suy --noconfirm &>>$INSTLOG

	echo -e "$CNT - Installing Media pacakges..."
	for SOFTWR in vlc stremio mpv spotify; do
		install_software $SOFTWR
	done
fi

### Install Gaming softwares ###
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to install your gaming softwares? (Y,n) ' GAM
if [[ $GAM == "N" || $GAM == "n" ]]; then
	echo -e "$COK - Packages won't be installed, moving on."
else
	echo -e "$CNT - Update the system..."
	sudo pacman -Suy --noconfirm &>>$INSTLOG

	echo -e "$CNT - Installing Gaming pacakges..."
	for SOFTWR in lutris steam wine-staging winetricks; do
		install_software $SOFTWR
	done
fi

### Install Displaylink ###
read -rep $'[\e[1;33mACTION\e[0m] - Would you like to install your displaylink softwares? (Y,n) ' DISP
if [[ $DISP == "N" || $DISP == "n" ]]; then
	echo -e "$COK - Packages won't be installed, moving on."
else
	echo -e "$CNT - Update the system..."
	sudo pacman -Suy --noconfirm &>>$INSTLOG
	echo -e "$CNT - Installing Displaylink pacakges..."
	if [[ "$LIN" == 2 ]]; then
		install_software evdi-compat-git
	else
		install_software evdi-git
	fi

	install_software displaylink
	sudo systemctl enable displaylink.service

	# Patch for hyprland
	if [[ $DE != "2" ]]; then
		echo -e "$CNT - Cloning hyprland with displaylink support."
			install_software hyprland-displaylink-git
	fi
fi

### Script is done ###
echo -e "$CNT - Script had completed!"
if [[ "$ISNVIDIA" == true ]]; then
	echo -e "$CAT - Since we attempted to setup Nvidia the script will now end and you should reboot.
    type 'reboot' at the prompt and hit Enter when ready."
	exit
fi

read -rep $'[\e[1;33mACTION\e[0m] - Would you like to start your DE now? (Y,n) ' HYP
if [[ $HYP == "N" || $HYP == "n" ]]; then
	exit
else
	if [[ $DE == "2" ]]; then
		exec sudo systemctl start gdm &>>$INSTLOG
	else
		exec sudo systemctl start sddm &>>$INSTLOG
	fi
fi
