--- Install lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

-- Configure lazy.nvim
require("lazy").setup({
	spec = {
		{ import = "colkain.plugins.colorscheme" },
		{ import = "colkain.plugins.lsp" },
		{ import = "colkain.plugins.utils" },
		{ import = "colkain.plugins.ui" },
		{ import = "colkain.plugins.other" },
	},
	defaults = { lazy = true, version = nil },
	-- install = { missing = true, colorscheme = { "catppuccin" } },
	dev = { patterns = jit.os:find("Windows") and {} or { "alpha2phi" } },
	checker = { enabled = true },
	performance = {
		cache = {
			enabled = true,
		},
		rtp = {
			disabled_plugins = {
				"gzip",
				"matchit",
				"netrwPlugin",
				"tarPlugin",
				"tohtml",
				"tutor",
				"zipPlugin",
			},
		},
	},
})
