local parsers = require("nvim-treesitter.parsers")

local function rep(s, n)
	local t = {}
	for i = 1, n do
		t[i] = s
	end
	print("s", s)
	return unpack(t, 1, n)
end

local all_langs = { "glsl", "bash", "html", "typescript", "javascript", "query", "regex" }

local function inject()
	local lang_tree = parsers.get_parser(0)
	local lang = lang_tree:lang()

	local query = ""

	if lang == "typescript" then
		for _, known_lang in ipairs(all_langs) do
			query = query
				.. string.format(--[[query]]
					[[
(
    (comment) @_comment (#contains? @_comment "%s")
    (
        (template_string) @%s
        (#offset! @%s 0 1 0 -1)
    )
)
(
    (comment) @_comment (#contains? @_comment "%s")
    (expression_statement
        (template_string) @%s
        (#offset! @%s 0 1 0 -1)
    )
)
]],
					rep(known_lang, 6)
				)
		end
	elseif lang == "javascript" then
		for _, known_lang in ipairs(all_langs) do
			query = query
				.. string.format(--[[query]]
					[[
(
    (comment) @_comment (#contains? @_comment "%s")
    (
        (template_string) @%s
        (#offset! @%s 0 1 0 -1)
    )
)
(
    (comment) @_comment (#contains? @_comment "%s")
    (expression_statement
        (template_string) @%s
        (#offset! @%s 0 1 0 -1)
    )
)
]],
					rep(known_lang, 6)
				)
		end
	elseif lang == "tsx" then
		for _, known_lang in ipairs(all_langs) do
			query = query
				.. string.format(--[[query]]
					[[
(
    (comment) @_comment (#contains? @_comment "%s")
    (
        (template_string) @%s
        (#offset! @%s 0 1 0 -1)
    )
)
(
    (comment) @_comment (#contains? @_comment "%s")
    (expression_statement
        (template_string) @%s
        (#offset! @%s 0 1 0 -1)
    )
)
	]],
					rep(known_lang, 6)
				)
		end
	elseif lang == "yaml" then
		for _, known_lang in ipairs(all_langs) do
			query = query
				.. string.format(--[[query]]
					[[
(flow_sequence
    (comment) @comment (#contains? @comment "%s")
    (
        (flow_node  
          (double_quote_scalar) @%s
          (#offset! @%s 0 1 0 -1)
        )      
    )
)
	]],
					rep(known_lang, 6)
				)
		end
	else
		print("Please PR this language!")
	end

	require("vim.treesitter.query").set(lang, "injections", query)
end

vim.api.nvim_create_user_command("Inject", function()
	vim.cmd(":w")
	inject()
	vim.cmd(":e")
end, {})
