return {
	"nvim-tree/nvim-tree.lua",
	dependencies = {
		"nvim-tree/nvim-web-devicons",
	},
	cmd = { "NvimTreeToggle", "NvimTreeFocus", "NvimTreeCollapse" },
	config = function()
		require("nvim-tree").setup({
			update_cwd = true,
			update_focused_file = {
				enable = true,
				update_cwd = true,
			},
			renderer = {
				highlight_git = true,
				icons = {
					show = {
						git = true,
					},
				},
			},
			git = {
				enable = true,
			},
			diagnostics = {
				enable = true,
			},
			filters = {
				dotfiles = false,
			},
		})
	end,
}
