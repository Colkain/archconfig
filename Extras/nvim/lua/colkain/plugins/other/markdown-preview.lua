return {
  "iamcco/markdown-preview.nvim",
  event = "BufEnter *.md",
  config = function()
    vim.g.mkdp_auto_close = false
    vim.g.mkdp_theme = "dark"
    vim.fn["mkdp#util#install"]()
  end,
}
