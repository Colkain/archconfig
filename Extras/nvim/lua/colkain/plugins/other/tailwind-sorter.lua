return {
  "laytan/tailwind-sorter.nvim",
  ft = { "html", "javascript", "typescript", "javascriptreact", "typescriptreact" },
  dependencies = { "nvim-treesitter/nvim-treesitter", "nvim-lua/plenary.nvim" },
  build = "cd formatter && npm install && npm run build",
  config = function()
    require("tailwind-sorter").setup({
      on_save_enabled = true,                                -- If `true`, automatically enables on save sorting.
      on_save_pattern = { "*.html", "*.js", "*.jsx", "*.tsx" }, -- The file patterns to watch and sort.
      node_path = "node",
    })
  end,
}
