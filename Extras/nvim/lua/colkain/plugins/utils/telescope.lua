return {
  {
    "nvim-telescope/telescope.nvim",
    event = "VeryLazy",
    dependencies = "nvim-lua/plenary.nvim",
    -- `opts` is not used because telescope needs to be loaded prior to the config being evaluated.
    config = function()
      local ts_actions = require("telescope.actions")
      local lga_actions = require("telescope-live-grep-args.actions")

      require("telescope").setup({
        defaults = {
          cache_picker = false,
          mappings = {
            i = {
              ["<C-Down>"] = ts_actions.cycle_history_next,
              ["<C-Up>"] = ts_actions.cycle_history_prev,
            },
          },
        },
        pickers = {
          buffers = {
            theme = "ivy",
          },
        },
        extensions = {
          ["ui-select"] = {
            require("telescope.themes").get_dropdown({}),
          },
          live_grep_args = {
            auto_quoting = true,
            mappings = {
              i = {
                ["<C-k>"] = lga_actions.quote_prompt(),
              },
            },
          },
        },
      })

      pcall(require("telescope").load_extension, "fzf")
      require("telescope").load_extension("ui-select")
      require("telescope").load_extension("smart_history")

      -- Telescope LSP mappings
      local augroup = vim.api.nvim_create_augroup("telescope-lsp", {})
      vim.api.nvim_create_autocmd("LspAttach", {
        desc = "Telescope LSP keybindings",
        group = augroup,
        callback = function(args)
          vim.keymap.set("n", "<leader>fS", function()
            require("telescope.builtin").lsp_document_symbols()
          end, {
            buffer = args.buf,
            desc = "Telescope find document symbols",
          })
          vim.keymap.set("n", "<leader>fs", function()
            require("telescope.builtin").lsp_dynamic_workspace_symbols()
          end, {
            buffer = args.buf,
            desc = "Telescope find workspace symbols",
          })
        end,
      })
    end,
  },

  {
    "nvim-telescope/telescope-fzf-native.nvim",
    build = "make",
    lazy = true,
    dependencies = "nvim-telescope/telescope.nvim",
  },

  {
    "nvim-telescope/telescope-ui-select.nvim",
    lazy = true,
    dependencies = "nvim-telescope/telescope.nvim",
  },

  {
    "nvim-telescope/telescope-smart-history.nvim",
    lazy = true,
    dependencies = { "nvim-telescope/telescope.nvim" },
  },

  {
    "nvim-telescope/telescope-live-grep-args.nvim",
    lazy = true,
    dependencies = "nvim-telescope/telescope.nvim",
  },
}
