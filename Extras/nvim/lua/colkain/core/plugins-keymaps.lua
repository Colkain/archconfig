-- defaults
local opts = { noremap = true, silent = true }
local map = vim.keymap.set

-- Map leader to <Space>
vim.g.mapleader = " "
map("n", " ", "<Nop>", { silent = true, remap = false })

-- Installers
map("n", "<leader>zz", "<cmd>:Lazy<cr>", { desc = "Manage Plugins" })
map("n", "<leader>zm", "<cmd>:Mason<cr>", { desc = "Open Mason" })

-- Nvim Tree
map("", "<Leader>e", "<Cmd>NvimTreeFocus<CR>", { desc = "Open Tree", noremap = true, silent = true })
map("", "<C-n>", "<Cmd>NvimTreeToggle<CR>", { desc = "Toggle Tree", noremap = true, silent = true })

-- Telescope
map("", "<Leader>ff", function()
	require("telescope.builtin").find_files()
end, { desc = "Find files", noremap = true, silent = true })
map("", "<Leader>fF", function()
	require("telescope.builtin").git_files()
end, { desc = "Find git files", noremap = true, silent = true })
map("", "<Leader>fw", function()
	require("telescope").extensions.live_grep_args.live_grep_args()
end, { desc = "Find word", noremap = true, silent = true })
map("", "<Leader>fh", function()
	require("telescope.builtin").help_tags()
end, { desc = "Telescope help tags", noremap = true, silent = true })
map("", "<Leader>fd", function()
	require("telescope.builtin").diagnostics()
end, { desc = "Telescope diagnostics", noremap = true, silent = true })
map("", "<Leader>fr", function()
	require("telescope.builtin").oldfiles()
end, { desc = "Find recent files", noremap = true, silent = true })
map("", "<Leader>fq", function()
	require("telescope.builtin").quickfix()
end, { desc = "Telescope quickfix", noremap = true, silent = true })

-- Nvim LSP Confing
map({ "n", "v" }, "<leader>ca", function()
	vim.lsp.buf.code_action({ apply = true })
end, { desc = "Code action", noremap = true, silent = true })
map("n", "gR", "<cmd>Telescope lsp_references<CR>", { desc = "Show LSP references", noremap = true, silent = true })
map("n", "gD", "<cmd>Telescope lsp_definitions<CR>", { desc = "Go to declaration", noremap = true, silent = true })
map("n", "gd", "<cmd>Telescope lsp_definitions<CR>", { desc = "Show LSP definitions", noremap = true, silent = true })
map(
	"n",
	"gi",
	"<cmd>Telescope lsp_implementations<CR>",
	{ desc = "Show LSP implementations", noremap = true, silent = true }
)
map(
	"n",
	"gt",
	"<cmd>Telescope lsp_type_definitions<CR>",
	{ desc = "Show LSP type definitions", noremap = true, silent = true }
)
map("n", "<leader>rn", vim.lsp.buf.rename, { desc = "Smart rename", noremap = true, silent = true })
map(
	"n",
	"<leader>D",
	"<cmd>Telescope diagnostics bufnr=0<CR>",
	{ desc = "Show LSP references", noremap = true, silent = true }
)
map("n", "<leader>d", vim.diagnostic.open_float, { desc = "Show line diagnostics", noremap = true, silent = true })
map("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic", noremap = true, silent = true })
map("n", "]d", vim.diagnostic.goto_next, {
	desc = "Go to next diagnostic",
	noremap = true,
	silent = true,
})
map(
	"n",
	"K",
	vim.lsp.buf.hover,
	{ desc = "Show documentation for what is under cursor", noremap = true, silent = true }
)
map("n", "<leader>rs", "<cmd>LspRestart<CR>", { desc = "Restart LSP", noremap = true, silent = true })

-- Nvim Notify
map("", "<leader>cl", function()
	require("notify").dismiss({ silent = true })
end, { desc = "Clear notifications", noremap = true, silent = true })

-- Markdown Preview
map("", "<C-p>", "<cmd>MarkdownPreviewToggle<cr>", { desc = "Preview markdown", noremap = true, silent = true })

-- TODO Comments
map("", "<leader>td", "<cmd>TodoTelescope<cr>", { desc = "Todo Telescope", noremap = true, silent = true })

-- Lazygit
map("", "<leader>gg", "<cmd>LazyGit<cr>", { desc = "Open LazyGit", noremap = true, silent = true })

-- Flash
map({ "n", "o", "x" }, "s", function()
	require("flash").jump()
end, opts)
map({ "n", "o", "x" }, "S", function()
	require("flash").treesitter()
end, opts)
map({ "o", "x" }, "R", function()
	require("flash").treesitter_search()
end, opts)

-- UFO
map("", "zR", function()
	require("ufo").openAllFolds()
end, { desc = "Open folds", noremap = true, silent = true })
map("", "zM", function()
	require("ufo").closeAllFolds()
end, { desc = "Close folds", noremap = true, silent = true })

-- Toggleterm
map("", "<Leader>tt", "<Cmd>ToggleTerm<CR>", { desc = "Open terminal", noremap = true, silent = true })
