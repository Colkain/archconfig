-- defaults
local opts = { noremap = true, silent = true }
local map = vim.keymap.set

-- Map leader to <Space>
vim.g.mapleader = " "
map("n", " ", "<Nop>", { silent = true, remap = false })
map("n", "<Esc>", "<Cmd>noh<CR>", { silent = true, remap = false })

-- Close Neovim
map("", "<Leader>qq", "<Cmd>qa!<CR>", { desc = "Quit Neovim Without saving", noremap = true, silent = true })
map("", "<Leader>qw", "<Cmd>wqa<CR>", { desc = "Save and quit Neovim", noremap = true, silent = true })

-- Buffer
map("", "<Leader>bn", "<Cmd>enew<CR>", { desc = "New buffer", noremap = true, silent = true })
map("", "<Leader>x", "<Cmd>bp<bar>sp<bar>bn<bar>bd<CR>", { desc = "Close buffer", noremap = true, silent = true })
map({ "n", "i" }, "<C-s>", "<Cmd>w<CR>", { desc = "Save file", noremap = true, silent = true })

-- Buffer Navigation
map("", "<tab>", "<Cmd>bn<CR>", { desc = "Go to next buffer", noremap = true, silent = true })
map("", "<S-tab>", "<Cmd>bp<CR>", { desc = "Go to previous buffer", noremap = true, silent = true })

-- Windows
map("", "<Leader>h", "<Cmd>split<CR>", { desc = "Horizontal split", noremap = true, silent = true })
map("", "<Leader>v", "<Cmd>vsp<CR>", { desc = "Vertical split", noremap = true, silent = true })
map("", "<C-Up>", "<Cmd>resize +5<CR>", { desc = "Vertical resize up", noremap = true, silent = true })
map("", "<C-Down>", "<Cmd>resize -5<CR>", { desc = "Vertical resize down", noremap = true, silent = true })
map("", "<C-Right>", "<Cmd>vertical resize +5<CR>", { desc = "Horizontal resize up", noremap = true, silent = true })
map("", "<C-Left>", "<Cmd>vertical resize -5<CR>", { desc = "Horizontal resize down", noremap = true, silent = true })

-- Window Navigation
map("", "<C-h>", "<C-w>h", { desc = "Go to left window", noremap = true, silent = true })
map("", "<C-l>", "<C-w>l", { desc = "Go to right window", noremap = true, silent = true })
map("", "<C-k>", "<C-w>k", { desc = "Go to top window", noremap = true, silent = true })
map("", "<C-j>", "<C-w>j", { desc = "Go to bottom window", noremap = true, silent = true })

-- Yanking
map("v", "x", "x", { desc = "Delete line with yanking", noremap = true, silent = true })
map("v", "d", '"_d', { desc = "Delete line", noremap = true, silent = true })
map("n", "d", "V", { desc = "Select line", noremap = true, silent = true })
map("n", "D", '"_dd', { desc = "Delete line", noremap = true, silent = true })
map("n", "x", '"_x', { desc = "Delete character", noremap = true, silent = true })

-- make the cursor stay on the same character when leaving insert mode
map("i", "<Esc>", "<Esc>l", opts)

-- fast scrolling
map("n", "K", "9j", opts)
map("n", "L", "9k", opts)
map("v", "K", "9j", opts)
map("v", "L", "9k", opts)

-- stay in normal mode after inserting a new line
map("", "o", "o <Bs><Esc>", opts)
map("", "O", "O <Bs><Esc>", opts)

-- indent via Tab
map("i", "<Tab>", "\t", opts)
map("i", "<S-Tab>", "\b", opts)

-- line numbers
map("", "<Leader>lr", "<Cmd>set rnu!<CR>", { desc = "Toggle relative line numbers", noremap = true, silent = true })
