#!/bin/sh
down() {
brightnessctl set 5%-
brightness=$(( 100 * $(brightnessctl get) / $(brightnessctl max) )) 
[ $brightness -gt 0 ] && brightness=`expr $brightness`  
dunstify -a "Brightness" "Decreasing to $brightness%" -h int:value:"$brightness" -i display-brightness-low-symbolic -r 2594 -u normal
}

up() {
brightnessctl set 5%+
brightness=$(( 100 * $(brightnessctl get) / $(brightnessctl max) )) 
[ $brightness -gt 0 ] && brightness=`expr $brightness`  
dunstify -a "Brightness" "Increasing to $brightness%" -h int:value:"$brightness" -i display-brightness-high-symbolic -r 2594 -u normal
}

case "$1" in
  up) up;;
  down) down;;
esac
