#!/bin/sh
down() {
pamixer --default-source -d 2
volume=$(pamixer --default-source --get-volume)
[ $volume -gt 0 ] && volume=`expr $volume`  
dunstify -a "Microphone" "Decreasing to $volume%" -h int:value:"$volume" -i audio-input-microphone-low -r 2592 -u normal
}

up() {
pamixer --default-source -i 2
volume=$(pamixer --default-source --get-volume)
[ $volume -lt 100 ] && volume=`expr $volume`  
dunstify -a "Microphone" "Increasing to $volume%" -h int:value:"$volume" -i audio-input-microphone-high -r 2592 -u normal
}

mute() {
muted="$(pamixer --default-source --get-mute)"
if $muted; then
  pamixer --default-source -u
  dunstify -a "Microphone" "UNMUTED" -i audio-input-microphone-high -r 2592 -u normal
else 
  pamixer --default-source -m
  dunstify -a "Microphone" "MUTED" -i audio-input-microphone-muted -r 2592 -u normal
fi
}

case "$1" in
  up) up;;
  down) down;;
  mute) mute;;
esac
