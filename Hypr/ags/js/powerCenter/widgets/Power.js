import { Widget, Utils } from "../../imports.js";

const PowerRevealer = () =>
  Widget.Revealer({
    class_name: "power",
    reveal_child: false,
    transition_duration: 500,
    transition: "slide_down",
    child: Widget.Box({
      class_name: "power",
      vertical: true,
      children: [
        Widget.Button({
          class_name: "power_header",
          onPrimaryClickRelease: () => Utils.exec("systemctl suspend"),
          child: Widget.Box({
            class_name: "power",
            children: [
              Widget.Icon({ icon: "system-suspend", size: 16 }),
              Widget.Label(" Suspend"),
            ],
          }),
        }),
        Widget.Button({
          class_name: "power_header",
          onPrimaryClickRelease: () => Utils.exec('pkill -KILL -u "$USER"'),
          child: Widget.Box({
            class_name: "power",
            children: [
              Widget.Icon({ icon: "system-log-out", size: 16 }),
              Widget.Label(" Logout"),
            ],
          }),
        }),
        Widget.Button({
          class_name: "power_header",
          onPrimaryClickRelease: () => Utils.exec("systemctl reboot"),
          child: Widget.Box({
            class_name: "power",
            children: [
              Widget.Icon({ icon: "system-restart", size: 16 }),
              Widget.Label(" Restart"),
            ],
          }),
        }),
        Widget.Button({
          class_name: "power_header",
          onPrimaryClickRelease: () => Utils.exec("systemctl poweroff"),
          child: Widget.Box({
            class_name: "power",
            children: [
              Widget.Icon({ icon: "system-shutdown", size: 16 }),
              Widget.Label(" Poweroff"),
            ],
          }),
        }),
      ],
    }),
  });

export default () => {
  const revealer = PowerRevealer();
  var arrow_icon = Widget.Icon({
    hpack: "end",
    icon: "arrow-down",
    size: 16,
  });
  return Widget.Box({
    vertical: true,
    children: [
      Widget.Button({
        class_name: "power_header",
        onPrimaryClickRelease: () => {
          arrow_icon.icon =
            arrow_icon.icon === "arrow-down" ? "arrow-up" : "arrow-down";
          revealer.reveal_child = !revealer.reveal_child;
        },
        child: Widget.Box({
          class_name: "power",
          orientation: "horizontal",
          hexpand: true,
          children: [
            Widget.Box({
              hexpand: true,
              hpack: "start",
              children: [
                Widget.Icon({ icon: "system-shutdown", size: 16 }),
                Widget.Label(" Power"),
              ],
            }),
            arrow_icon,
          ],
        }),
      }),
      revealer,
    ],
  });
};
