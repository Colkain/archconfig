import App from "resource:///com/github/Aylur/ags/app.js";
import { Widget, Variable, Utils, App as Application } from "../../imports.js";

const updateSys = Variable(
  {},
  {
    poll: [100000, `bash ${App.configDir}/bin/updateSys.sh`],
  },
);

export default () =>
  Widget.Button({
    class_name: "update_system",
    onPrimaryClickRelease: () => {
      Application.closeWindow("powercenter");
      Utils.exec(`bash ${App.configDir}/bin/updateSys.sh update`);
    },
    child: Widget.Label({
      binds: [
        [
          "label",
          updateSys,
          "value",
          (value) => (Object.keys(value).length !== 0 ? value.toString() : "󰇘"),
        ],
      ],
    }),
  });
