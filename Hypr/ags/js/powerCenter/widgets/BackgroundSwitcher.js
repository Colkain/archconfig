import { App, Widget, Utils } from "../../imports.js";

export default () =>
  Widget.Button({
    class_name: "bg_header",
    onPrimaryClickRelease: () =>
      Utils.execAsync(`bash -c ${App.configDir}/bin/randomWallpaper`),
    child: Widget.Box({
      class_name: "bg_theme)",
      children: [
        Widget.Icon({
          icon: "applications-graphics-symbolic",
          size: 16,
        }),
        Widget.Label(" Switch"),
      ],
    }),
  });
