import { Widget } from "../imports.js";
import PopupWindow from "../misc/PopupWindow.js";

import UpdateSys from "./widgets/UpdateSys.js";
import Power from "./widgets/Power.js";
import BackgroundSwitcher from "./widgets/BackgroundSwitcher.js";

export default () =>
  PopupWindow({
    name: "powercenter",
    anchor: ["top", "right"],
    layout: "top",
    content: Widget.Box({
      class_name: "power_container",
      vertical: true,
      children: [UpdateSys(), BackgroundSwitcher(), Power()],
    }),
  });
