import { Widget, App, Utils } from "../../imports.js";

/** @param {import('resource:///com/github/Aylur/ags/service/applications.js').Application} app */
export default (app) => {
  const title = Widget.Label({
    class_name: "title",
    label: app.text || "",
    truncate: "end",
    maxWidthChars: 60,
    hpack: "start",
    hexpand: true,
  });

  const id = Widget.Label({
    class_name: "description",
    label: app.id,
    hpack: "end",
    hexpand: true,
  });

  const textBox = Widget.Box({
    class_name: "clipboard-textbox",
    hexpand: true,
    orientation: "horizontal",
    children: app.id ? [title, id] : [title],
  });

  return Widget.Button({
    class_name: "clipboard-item",
    hexpand: true,
    setup: (self) => (self.app = app),
    on_clicked: () => {
      Utils.execAsync(`wl-copy "${app.text}"`);
      App.closeWindow("clipboardcenter");
    },
    child: textBox,
  });
};
