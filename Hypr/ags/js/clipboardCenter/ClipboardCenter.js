import { Widget, App, Utils } from "../imports.js";
import Separator from "../misc/Separator.js";
import PopupWindow from "../misc/PopupWindow.js";
import icons from "../icons.js";
import ClipboardItem from "./widgets/ClipboardItem.js";

const WINDOW_NAME = "clipboardcenter";

const input = Utils.exec("cliphist list");
const lines = input.split("\n");

// Define a regular expression pattern for the first type of line
const type1Pattern = /^(\d+)\s+(.+)/;

// Process each line and create JSON objects
const jsonObjects = lines
  .map((line) => {
    const type1Match = line.match(type1Pattern);
    if (type1Match) return { id: type1Match[1], text: type1Match[2] };
    return null; // Handle lines that don't match either pattern
  })
  .filter(Boolean); // Remove null entries

const Clipboard = () => {
  const list = Widget.Box({ vertical: true });

  const placeholder = Widget.Label({
    label: "  Couldn't find a match",
    class_name: "placeholder",
  });

  const entry = Widget.Entry({
    hexpand: true,
    text: "-",
    placeholder_text: "Search",
    on_accept: ({ text }) => {
      const list = jsonObjects.filter((item) => item.text.includes(text));
      if (list[0]) App.toggleWindow(WINDOW_NAME);
    },
    on_change: ({ text }) => {
      list.children = jsonObjects
        .filter((item) => item.text.includes(text))
        .map((app) => [Separator(), ClipboardItem(app)])
        .flat();
      list.add(Separator());
      list.show_all();

      placeholder.visible = list.children.length === 1;
    },
  });

  return Widget.Box({
    class_name: "clipboard",
    properties: [["list", list]],
    vertical: true,
    children: [
      Widget.Box({
        class_name: "clipboard_header",
        children: [Widget.Icon(icons.apps.search), entry],
      }),
      Widget.Scrollable({
        hscroll: "never",
        child: Widget.Box({
          vertical: true,
          children: [list, placeholder],
        }),
      }),
    ],
    connections: [
      [
        App,
        (_, name, visible) => {
          if (name !== WINDOW_NAME) return;

          entry.set_text("");
          if (visible) entry.grab_focus();
        },
      ],
    ],
  });
};

export default () =>
  PopupWindow({
    name: WINDOW_NAME,
    layout: "top",
    content: Clipboard(),
  });
