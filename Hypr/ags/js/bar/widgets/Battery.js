import { App, Widget } from "../../imports.js";
import Battery from "resource:///com/github/Aylur/ags/service/battery.js";
import * as battery from "../../misc/battery.js";

export default () =>
  Widget.Button({
    class_name: "battery_header",
    hpack: "end",
    on_clicked: () => App.toggleWindow("powercenter"),
    child: Widget.Box({
      class_names: [
        "battery",
        battery.getDetails().charging
          ? "battery_charging"
          : "battery_discharging",
      ],
      children: [battery.Indicator(), battery.LevelLabel()],
    }),
    connections: [
      [
        App,
        (btn, win, visible) => {
          btn.toggleClassName("active", win === "powercenter" && visible);
        },
      ],
      [
        Battery,
        (btn) => {
          btn.toggleClassName(
            "battery_full",
            Battery.percent >= 80 && Battery.charging,
          );
          btn.toggleClassName(
            "battery_low",
            Battery.percent <= 30 && !Battery.charging,
          );
          btn.toggleClassName(
            "battery_critical",
            Battery.percent <= 15 && !Battery.charging,
          );
        },
      ],
    ],
  });
