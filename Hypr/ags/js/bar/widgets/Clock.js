import GLib from "gi://GLib";

import { App, Widget } from "../../imports.js";

export default ({
  format = "%a %d %b, %H:%M",
  interval = 1000,
  ...props
} = {}) =>
  Widget.Button({
    on_clicked: () => App.toggleWindow("notificationscenter"),
    class_name: "clock",
    connections: [
      [
        interval,
        (label) => (label.label = GLib.DateTime.new_now_local().format(format)),
      ],
      [
        App,
        (btn, win, visible) => {
          btn.toggleClassName(
            "active",
            win === "notificationscenter" && visible,
          );
        },
      ],
    ],
    ...props,
  });
