import { App, Widget } from "../../imports.js";

export default () =>
  Widget.Button({
    class_name: "launcher",
    on_primary_click: () => App.toggleWindow("applauncher"),
    child: Widget.Box({
      class_name: "launcher_icon",
      vpack: "center",
      hpack: "center",
      hexpand: true,
      vexpand: true,
    }),
    connections: [
      [
        App,
        (btn, win, visible) => {
          btn.toggleClassName(
            "launcher_active",
            win === "applauncher" && visible,
          );
        },
      ],
    ],
  });
