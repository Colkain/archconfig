import Clock from "./widgets/Clock.js";
import Workspaces from "./widgets/Workspaces.js";
import Separator from "../misc/Separator.js";
import { Widget } from "../imports.js";

import { ActiveApp } from "./widgets/ActiveApp.js";
import SystemIndicators from "./widgets/SystemIndicators.js";
import Battery from "./widgets/Battery.js";
import Launcher from "./widgets/Launcher.js";

const Left = () =>
  Widget.Box({
    class_name: "bar_left",
    orientation: "horizontal",
    hpack: "start",
    hexpand: true,
    children: [Launcher(), Separator(), Workspaces(), Separator(), ActiveApp()],
  });

const Center = () =>
  Widget.Box({
    class_name: "bar_center",
    child: Clock(),
  });

const Right = () =>
  Widget.Box({
    class_name: "bar_right",
    orientation: "horizontal",
    hpack: "end",
    children: [SystemIndicators(), Battery()],
  });

export default (monitor) =>
  Widget.Window({
    name: `bar${monitor}`,
    anchor: ["top", "left", "right"],
    exclusivity: "exclusive",
    monitor,
    hexpand: true,
    child: Widget.CenterBox({
      class_name: "bar",
      startWidget: Left(),
      centerWidget: Center(),
      endWidget: Right(),
    }),
  });
