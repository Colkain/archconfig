import * as mpris from "../../misc/mpris.js";
import { Mpris, Widget } from "../../imports.js";
import Separator from "../../misc/Separator.js";

const PlayerBox = (player) =>
  Widget.Box({
    class_name: "player-details",
    orientation: "vertical",
    vertical: true,
    children: [
      mpris.TitleLabel(player, {
        class_name: "title",
        truncate: "end",
        maxWidthChars: 24,
        hpack: "start",
        wrap: false,
      }),
      mpris.ArtistLabel(player, {
        class_name: "artist",
        truncate: "end",
        maxWidthChars: 24,
        hpack: "start",
        wrap: false,
      }),
      Widget.Box({
        class_name: "player-buttons-header",
        orientation: "horizontal",
        hexpand: true,
        children: [
          Widget.Box({
            class_name: "player-buttons-position",
            orientation: "horizontal",
            hpack: "start",
            children: [
              mpris.PositionSlider(player),
              Separator(),
              mpris.ShuffleButton(player),
              Separator(),
              mpris.LoopButton(player),
            ],
          }),
          Widget.Box({
            class_name: "player-buttons",
            orientation: "horizontal",
            hpack: "end",
            children: [
              mpris.PreviousButton(player),
              Separator(),
              mpris.PlayPauseButton(player),
              Separator(),
              mpris.NextButton(player),
            ],
          }),
        ],
      }),
    ],
  });

export default () =>
  Widget.Box({
    class_name: "media",
    vertical: true,
    properties: [["players", new Map()]],
    binds: [["children", Mpris, "players", (ps) => ps.map(PlayerBox)]],
  });
