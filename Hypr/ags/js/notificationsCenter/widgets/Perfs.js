import { Widget, Variable, Utils, App as Application } from "../../imports.js";
import Separator from "../../misc/Separator.js";

const divide = ([total, free]) => free / total;

const cpu = Variable(0, {
  poll: [
    2000,
    "top -b -n 1",
    (out) =>
      `CPU:  ${
        Math.round(
          divide([
            100,
            out
              .split("\n")
              .find((line) => line.includes("Cpu(s)"))
              .split(/\s+/)[1]
              .replace(",", "."),
          ]) * 1000,
        ) / 10
      }%`,
  ],
});

const ram = Variable(0, {
  poll: [
    2000,
    "free",
    (out) =>
      `RAM:  ${
        Math.round(
          divide(
            out
              .split("\n")
              .find((line) => line.includes("Mem:"))
              .split(/\s+/)
              .splice(1, 2),
          ) * 1000,
        ) / 10
      }%`,
  ],
});

const CpuProgress = () =>
  Widget.Label({
    class_name: "perfs",
    binds: [["label", cpu, "value", (out) => out.toString()]],
  });

const RamProgress = () =>
  Widget.Label({
    class_name: "perfs",
    binds: [["label", ram]],
  });

export default () =>
  Widget.Button({
    class_name: "perfs_header",
    hpack: "center",
    onPrimaryClickRelease: () => {
      Application.closeWindow("notificationscenter");
      Utils.exec("kitty --detach -e btop");
    },
    child: Widget.Box({
      children: [CpuProgress(), Separator(), RamProgress()],
    }),
  });
