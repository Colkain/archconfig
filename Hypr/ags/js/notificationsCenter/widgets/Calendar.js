import { Widget } from "../../imports.js";

export default () =>
  Widget.Box({
    class_name: "calendar",
    children: [
      Widget.Calendar({
        hexpand: true,
        hpack: "start",
      }),
    ],
  });
