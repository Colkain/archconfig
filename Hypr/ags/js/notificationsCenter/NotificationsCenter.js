import { Widget } from "../imports.js";
import Separator from "../misc/Separator.js";
import PopupWindow from "../misc/PopupWindow.js";

import NotificationColumn from "./widgets/NotificationsColumn.js";
import Calendar from "./widgets/Calendar.js";
import Weather from "./widgets/Weather.js";
import Media from "./widgets/Media.js";
import Perfs from "./widgets/Perfs.js";

export default () =>
  PopupWindow({
    name: "notificationscenter",
    anchor: ["top", "left", "right"],
    layout: "top",
    content: Widget.Box({
      class_name: "notification_container",
      children: [
        Widget.Box({
          class_name: "calendar_header",
          vertical: true,
          children: [
            Weather(),
            Separator(),
            Calendar(),
            Separator(),
            Perfs(),
            Separator(),
            Media(),
          ],
        }),
        NotificationColumn(),
      ],
    }),
  });
