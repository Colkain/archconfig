import { USER } from "resource:///com/github/Aylur/ags/utils.js";
import Bar from "./js/bar/Bar.js";
import Notifications from "./js/notifications/Notifications.js";
import ControlCenter from "./js/controlCenter/ControlCenter.js";
import NotificationsCenter from "./js/notificationsCenter/NotificationsCenter.js";
import PowerCenter from "./js/powerCenter/PowerCenter.js";
import Applauncher from "./js/applauncher/Applauncher.js";
import OSD from "./js/osd/OSD.js";

import { forMonitors, globalServices, warnOnLowBattery } from "./js/utils.js";
import { BluetoothDevices } from "./js/controlCenter/widgets/Bluetooth.js";
import { WifiSelection } from "./js/controlCenter/widgets/Network.js";
import { AppMixer } from "./js/controlCenter/widgets/Volume.js";
import { SinkSelector } from "./js/controlCenter/widgets/Volume.js";
import ClipboardCenter from "./js/clipboardCenter/ClipboardCenter.js";

warnOnLowBattery();
globalServices();

const windows = () => [
  forMonitors(Bar),
  forMonitors(Notifications),
  forMonitors(OSD),
  Applauncher(),
  ClipboardCenter(),
  NotificationsCenter(),
  ControlCenter(),
  PowerCenter(),
  BluetoothDevices(),
  WifiSelection(),
  AppMixer(),
  SinkSelector(),
];

export default {
  windows: windows().flat(2),
  maxStreamVolume: 1.5,
  cacheNotificationActions: true,
  closeWindowDelay: {
    quicksettings: 300,
    dashboard: 300,
  },
  style: `/home/${USER}/.config/ags/style.css`,
  notificationPopupTimeout: 5000, // milliseconds
};
